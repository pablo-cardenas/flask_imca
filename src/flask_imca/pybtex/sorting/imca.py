from pybtex.style.sorting import BaseSortingStyle


class SortingStyle(BaseSortingStyle):
    def sorting_key(self, entry):
        return (entry.fields.get("year", ""), entry.fields.get("title", ""))

    def sort(self, entries):
        return sorted(entries, key=self.sorting_key, reverse=True)
