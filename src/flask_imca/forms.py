import re

from flask_babel import _
from flask_wtf import RecaptchaField  # type: ignore
from markupsafe import Markup
from pybtex.database import parse_string  # type: ignore
from pybtex.plugin import find_plugin
from pybtex.scanner import PybtexSyntaxError  # type: ignore
from wtforms import DateTimeLocalField
from wtforms import FileField
from wtforms import Form
from wtforms import FormField
from wtforms import HiddenField
from wtforms import StringField
from wtforms import TextAreaField
from wtforms.validators import InputRequired
from wtforms.validators import ValidationError


class FileRegex:
    def __init__(self, regex, flags=0, message=None) -> None:
        regex = re.compile(regex, flags)
        self.regex = regex
        self.message = message

    def __call__(self, form, field, message=None) -> bool:
        match = self.regex.match(field.data.filename or "")
        if not match:
            if message is None:
                if self.message is None:
                    message = field.gettext("Invalid file.")
                else:
                    message = self.message

            raise ValidationError(message)
        return match


class Bibtex:
    def __init__(self, message=None):
        self.message = message

    def __call__(self, form, field, message=None):
        try:
            bib = parse_string(field.data, "bibtex")

            APA = find_plugin("pybtex.style.formatting", "apa")()
            formatted_bib = APA.format_bibliography(bib)

            HTML = find_plugin("pybtex.backends", "html")()

            for entry in formatted_bib.entries:
                entry.text.render(HTML)

        except PybtexSyntaxError as inst:
            raise ValidationError(inst)

        except AttributeError as inst:
            raise ValidationError(inst)

        except Exception as inst:
            raise ValidationError(inst)


class MultilingualMarkdownForm(Form):
    english = TextAreaField(
        validators=[],
        render_kw={
            "class": "simplemde",
        },
    )
    spanish = TextAreaField(
        validators=[],
        render_kw={
            "class": "simplemde",
        },
    )


class MultilingualTextAreaForm(Form):
    english = TextAreaField(
        validators=[InputRequired()],
        render_kw={
            "rows": "10",
        },
    )
    spanish = TextAreaField(
        validators=[InputRequired()],
        render_kw={
            "rows": "10",
        },
    )


class MultilingualStringForm(Form):
    english = StringField(
        validators=[InputRequired()],
    )
    spanish = StringField(
        validators=[InputRequired()],
    )


class ResearcherForm(Form):
    field = FormField(MultilingualStringForm)
    photo = FileField(
        _("Change Photo"),
        validators=[],
        description="""
Puede subir la foto en cualquier formato (PNG, JPG, ...).
No importa el tamaño de la foto ya que la foto pesadas serán modificada
  para que la página web sea más ligera.
El tamaño de la foto será de 225x300 (3:4).
La región que se mostrará en el perfil es
  el mayor rectángulo centrado 3:4 inscrito en la foto.
""",
    )
    summary = FormField(MultilingualMarkdownForm)
    publications = TextAreaField(
        "Publications",
        validators=[Bibtex()],
        render_kw={"rows": "15"},
        description=Markup(
            _("Publications in BibTeX format.")
            + ' <a href="https://en.wikibooks.org/wiki/'
            + 'LaTeX/Bibliography_Management#BibTeX">BibTeX</a>'
        ),
    )


class GraduatedStudentForm(Form):
    photo = FileField(
        _("Change Photo"),
        validators=[],
        description="""
Puede subir la foto en cualquier formato (PNG, JPG, ...).
No importa el tamaño de la foto ya que la foto pesadas serán modificada
  para que la página web sea más ligera.
El tamaño de la foto será de 225x300 (3:4).
La región que se mostrará en el perfil es
  el mayor rectángulo centrado 3:4 inscrito en la foto.
""",
    )
    summary = FormField(MultilingualMarkdownForm)


class ResearchAreaForm(Form):
    photo = FileField("Change Photo", validators=[])
    summary = FormField(MultilingualMarkdownForm)
    publications = TextAreaField(
        "Publications",
        validators=[Bibtex()],
        render_kw={"rows": "15"},
        description=Markup(
            _("Publications in BibTeX format.")
            + ' <a href="https://en.wikibooks.org/wiki/LaTeX/'
            + 'Bibliography_Management#BibTeX">BibTeX</a>'
        ),
    )


class FlatPageForm(Form):
    content = FormField(MultilingualMarkdownForm)


class ProgramForm(Form):
    body = FormField(MultilingualMarkdownForm)


class PostForm(Form):
    post_date = DateTimeLocalField(
        validators=[InputRequired()], format="%Y-%m-%dT%H:%M"
    )
    expiration_date = DateTimeLocalField(
        validators=[InputRequired()], format="%Y-%m-%dT%H:%M"
    )
    subtype = FormField(MultilingualStringForm)
    title = FormField(MultilingualStringForm)
    photo = FileField("Change Photo", validators=[])
    body = FormField(MultilingualMarkdownForm)


class NewForm(PostForm):
    subtitle = FormField(MultilingualStringForm)


class EventForm(PostForm):
    start_datetime = DateTimeLocalField(
        validators=[InputRequired()], format="%Y-%m-%dT%H:%M"
    )
    end_datetime = DateTimeLocalField(
        validators=[InputRequired()], format="%Y-%m-%dT%H:%M"
    )
    location = FormField(MultilingualStringForm)
    speaker = FormField(MultilingualStringForm)


class FileUploadForm(Form):
    file = FileField("Upload File", validators=[])


class RecaptchaForm(Form):
    recaptcha = RecaptchaField()
    url_next = HiddenField()
