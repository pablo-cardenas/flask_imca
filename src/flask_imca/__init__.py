""" Flask_IMCA.

IMCA website made with flask.
"""

__version__ = "0.1"

import os

from flask import Flask
from flask import g
from flask import redirect
from flask import request
from flask import url_for
from jinja2 import StrictUndefined
from pybtex.plugin import find_plugin


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="dev",
        DATABASE_URI="sqlite:///" + os.path.join(app.instance_path, "imca.db"),
        MEDIA_FOLDER=os.path.join(app.instance_path, "media"),
    )
    app.jinja_env.undefined = StrictUndefined

    if test_config is None:
        # load instance config, if exists, when not testing
        app.config.from_pyfile("config.py", silent=True)
    else:
        # load test config if passed in
        app.config.from_mapping(test_config)

    # Ensure the instance and media folders exist
    os.makedirs(app.instance_path, exist_ok=True)
    folder_names = ["teacher", "post", "research-area", "uploaded-files"]
    for folder_name in folder_names:
        folder_path = os.path.join(app.config["MEDIA_FOLDER"], folder_name)
        os.makedirs(folder_path, exist_ok=True)

    # Set the static folder
    if "STATIC_ROOT" in app.config:
        app.static_folder = app.config["STATIC_ROOT"]

    @app.route("/")
    def index():
        from flask_babel import get_locale

        g.lang_code = get_locale()
        return redirect(url_for("multilingual.index", lang_code=g.lang_code))

    @app.route("/<link_name>")
    def short_link(link_name):
        if link_name == "minicurso-antologia":
            link_url = (
                "/es/noticia/event"
                "/minicurso/una-antologia-de-ingenios-matematicos"
            )
            return redirect(link_url)
        elif link_name == "mmc":
            link_url = (
                "/es/programa"
                "/maestria-en-modelizacion-matematica-y-computacional"
            )
            return redirect(link_url)
        return redirect(url_for("index"))

    @app.context_processor
    def inject_categories():
        from .multilingual.models import FlatPage
        from .database import db_session
        from flask_babel import get_locale
        from sqlalchemy import select

        locale = get_locale()

        flatpages = db_session.execute(select(FlatPage)).scalars().all()

        return dict(flatpages=flatpages, locale=locale, view_args={})

    @app.template_filter("heading")
    def heading_filter(s: str, count):
        html = s
        for before in range(6 - count, 0, -1):
            after = before + count
            html = html.replace(f"<h{before}", f"<h{after}")
            html = html.replace(f"</h{before}>", f"</h{after}>")

        html = html.replace('<a href="', '<a target="_blank" href="')
        return html

    @app.template_filter("bib2apa")
    def bib2apa_filter(bib):
        APA = find_plugin("pybtex.style.formatting", "apa")(
            sorting_style="imca"
        )
        return APA.format_bibliography(bib)

    @app.template_filter("apa2html")
    def apa2html_filter(entry):
        HTML = find_plugin("pybtex.backends", "html")()
        return entry.text.render(HTML)

    from flask_babel import Babel

    def get_locale():
        from flask import g

        lang_code = g.get("lang_code")
        if lang_code is not None:
            return lang_code

        return request.accept_languages.best_match(["en", "es"])

    Babel(app, locale_selector=get_locale)

    from . import database

    database.init_app(app)

    from . import root_files

    app.register_blueprint(root_files.bp)

    from . import multilingual

    app.register_blueprint(multilingual.bp)
    multilingual.auth.oauth.init_app(app)

    return app
