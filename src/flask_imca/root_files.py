import os

from flask import Blueprint
from flask import current_app
from flask import send_from_directory


bp = Blueprint("root_files", __name__)


@bp.route("/android-chrome-192x192.png")
def android_chrome_192x192_png():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "android-chrome-192x192.png",
        mimetype="image/png",
    )


@bp.route("/android-chrome-512x512.png")
def android_chrome_512x512_png():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "android-chrome-512x512.png",
        mimetype="image/png",
    )


@bp.route("/apple-touch-icon.png")
def apple_touch_icon_png():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "apple-touch-icon.png",
        mimetype="image/png",
    )


@bp.route("/browserconfig.xml")
def bworserconfig_xml():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "browserconfig.xml",
        mimetype="application/xml",
    )


@bp.route("/favicon-16x16.png")
def favicon_16x16_png():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "favicon-16x16.png",
        mimetype="image/png",
    )


@bp.route("/favicon-32x32.png")
def favicon_32x32_png():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "favicon-32x32.png",
        mimetype="image/png",
    )


@bp.route("/favicon.ico")
def favicon_ico():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "favicon.ico",
        mimetype="image/vnd.microsoft.icon",
    )


@bp.route("/mstile-70x70.png")
def mstile_70x70_png():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "mstile-70x70.png",
        mimetype="image/png",
    )


@bp.route("/mstile-144x144.png")
def mstile_144x144_png():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "mstile-144x144.png",
        mimetype="image/png",
    )


@bp.route("/mstile-150x150.png")
def mstile_150x150_png():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "mstile-150x150.png",
        mimetype="image/png",
    )


@bp.route("/mstile-310x150.png")
def mstile_310x150_png():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "mstile-310x150.png",
        mimetype="image/png",
    )


@bp.route("/mstile-310x310.png")
def mstile_310x310_png():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "mstile-310x310.png",
        mimetype="image/png",
    )


@bp.route("/safari-pinned-tab.svg")
def safari_pinned_tab_svg():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "safari-pinned-tab.svg",
        mimetype="image/svg+xml",
    )


@bp.route("/site.webmanifest")
def site_webmanifest():
    return send_from_directory(
        os.path.join(current_app.root_path, current_app.static_folder),
        "site.webmanifest",
        mimetype="application/json",
    )
