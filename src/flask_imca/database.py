import click
from flask import current_app
from flask import Flask
from flask import g
from flask.cli import with_appcontext
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from sqlalchemy.pool import NullPool
from werkzeug.local import LocalProxy


def get_db():
    if "db_session" not in g:
        engine = create_engine(
            current_app.config["DATABASE_URI"],
            pool_pre_ping=True,
            poolclass=NullPool,
        )

        g.db_session = Session(engine)

    return g.db_session


db_session = LocalProxy(get_db)


def init_db(echo=True):
    engine = db_session.get_bind()
    engine.echo = echo

    from .models import Base

    Base.metadata.create_all(engine)


@click.command("init-db")
@with_appcontext
def init_db_command():
    """Initialize the database"""
    init_db()
    click.echo("Initialized the database.")


def teardown_session(exception=None):
    if "db_session" in g:
        g.db_session.close()
        g.db_session.invalidate()
        g.db_session.get_bind().dispose()


def init_app(app: Flask):
    app.teardown_appcontext(teardown_session)
    app.cli.add_command(init_db_command)
