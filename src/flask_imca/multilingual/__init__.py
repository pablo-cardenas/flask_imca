import locale
from datetime import datetime

from flask import Blueprint
from flask import g
from flask import render_template
from flask import request
from flask_babel import Locale
from sqlalchemy import select
from werkzeug.exceptions import abort

from flask_imca.database import db_session
from flask_imca.multilingual import auth
from flask_imca.multilingual import dashboard
from flask_imca.multilingual import file_upload
from flask_imca.multilingual import flatpage
from flask_imca.multilingual import people
from flask_imca.multilingual import post
from flask_imca.multilingual import program
from flask_imca.multilingual import recaptcha
from flask_imca.multilingual import research_area
from flask_imca.multilingual.models import Program
from flask_imca.multilingual.models import ResearchArea
from flask_imca.multilingual.post.models import Post

bp = Blueprint(
    "multilingual",
    __name__,
    template_folder="templates",
    url_prefix="/<string(length=2):lang_code>",
)

bp.register_blueprint(auth.bp)
bp.register_blueprint(recaptcha.bp)
bp.register_blueprint(post.bp)
bp.register_blueprint(flatpage.bp)
bp.register_blueprint(people.bp)
bp.register_blueprint(research_area.bp)
bp.register_blueprint(program.bp)
bp.register_blueprint(dashboard.bp)
bp.register_blueprint(file_upload.bp)


@bp.before_request
def before_request():
    if g.lang_code not in ["es", "en"]:
        abort(404)
    dfl = request.url_rule.defaults
    if "lang_code" in dfl:
        if dfl["lang_code"] != request.full_path.split("/")[1]:
            abort(404)


@bp.url_defaults
def add_language_code(endpoint, values):
    values.setdefault("lang_code", g.lang_code)


@bp.url_value_preprocessor
def pull_lang_code(endpoint, values):
    g.lang_code = values.pop("lang_code")


@bp.route("/")
def index():
    locale.setlocale(locale.LC_COLLATE, "en_US.UTF-8")
    research_areas = db_session.query(ResearchArea).all()
    research_areas.sort(
        key=lambda p: locale.strxfrm(p.language(Locale(g.lang_code)).name)
    )

    programs = db_session.execute(
        select(Program).filter_by(is_active=True)
    ).scalars()

    posts = [p for p in db_session.query(Post).all() if p.importance > 0]
    posts.sort(key=lambda p: p.importance, reverse=True)

    return render_template(
        "multilingual/index.html",
        research_areas=research_areas,
        programs=programs,
        posts=posts,
        datetime=datetime,
    )
