from babel import Locale
from flask import Blueprint
from flask import g
from flask import render_template
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound
from werkzeug.exceptions import abort

from flask_imca.database import db_session
from flask_imca.models import FileUploaded
from flask_imca.multilingual.auth import login_required
from flask_imca.multilingual.models import GraduatedStudent
from flask_imca.multilingual.models import Researcher
from flask_imca.multilingual.models import User
from flask_imca.multilingual.post.models import Post

bp = Blueprint("dashboard", __name__)


@bp.route("/dashboard")
@login_required
def index():
    if not g.user.is_admin:
        abort(403)

    posts = db_session.execute(select(Post)).scalars().all()
    files_uploaded = db_session.execute(select(FileUploaded)).scalars().all()
    users = db_session.execute(select(User)).scalars().all()

    posts.sort(key=lambda p: p.importance, reverse=True)
    users.sort(key=lambda u: (u.lastname1, u.lastname2, u.firstname))

    for user in users:
        try:
            db_session.execute(
                select(Researcher).filter_by(user=user)
            ).scalar_one()
        except NoResultFound:
            user.is_researcher = False
        else:
            user.is_researcher = True

        gs = (
            db_session.execute(select(GraduatedStudent).filter_by(user=user))
            .scalars()
            .all()
        )
        user.is_graduated_student = bool(gs)

    return render_template(
        "multilingual/dashboard/index.html",
        posts=posts,
        files_uploaded=files_uploaded,
        users=users,
        Locale=Locale,
    )
