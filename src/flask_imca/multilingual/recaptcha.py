import functools

from flask import Blueprint
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from flask_imca.forms import RecaptchaForm

bp = Blueprint("recaptcha", __name__)


@bp.route("/recaptcha", methods=("GET", "POST"))
def recaptcha():
    if request.method == "GET":
        form = RecaptchaForm()
        form.url_next.data = request.args.get("next")
    else:
        form = RecaptchaForm(request.form)

        if form.validate():
            session["recaptcha"] = True
            return redirect(form.url_next.data)

    return render_template("multilingual/recaptcha/recaptcha.html", form=form)


def recaptcha_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if "recaptcha" not in session:
            url_next = request.path
            return redirect(
                url_for("multilingual.recaptcha.recaptcha", next=url_next)
            )

        return view(**kwargs)

    return wrapped_view
