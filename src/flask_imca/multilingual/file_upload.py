import os

from flask import Blueprint
from flask import current_app
from flask import redirect
from flask import render_template
from flask import request
from flask import send_from_directory
from flask import url_for
from werkzeug.utils import secure_filename

from flask_imca.database import db_session
from flask_imca.forms import FileUploadForm
from flask_imca.models import FileUploaded

bp = Blueprint("file_upload", __name__, url_prefix="file_upload")


@bp.route("/upload", methods=("GET", "POST"))
def upload():
    if request.method == "GET":
        form = FileUploadForm()
    else:
        form = FileUploadForm(request.files)

        if form.validate():
            file = form.file.data
            filename = secure_filename(file.filename)
            path = os.path.join(
                current_app.config["MEDIA_FOLDER"], "uploaded-files", filename
            )

            if os.path.isfile(path):
                basename, extension = os.path.splitext(path)
                i = 0
                while True:
                    i += 1
                    new_path = f"{basename}-{i}{extension}"
                    if not os.path.isfile(new_path):
                        path = new_path
                        break

            file.save(path)

            uploaded_file = FileUploaded(
                filename=os.path.basename(path), mimetype=file.mimetype
            )
            db_session.add(uploaded_file)
            db_session.commit()

            return redirect(url_for("multilingual.dashboard.index"))

    return render_template("multilingual/file_upload/upload.html", form=form)


@bp.route("/file/<filename>", methods=("GET", "POST"))
def file(filename):
    return send_from_directory(
        directory=os.path.join(
            current_app.config["MEDIA_FOLDER"],
            "uploaded-files",
        ),
        path=filename,
        download_name=filename,
    )
