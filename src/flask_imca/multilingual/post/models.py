from datetime import datetime
from datetime import timedelta
from typing import List

from slugify import slugify
from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

from flask_imca.multilingual.models import Base
from flask_imca.multilingual.models import LanguageMixin


class Post(LanguageMixin, Base):
    __tablename__ = "post"
    id = Column(Integer, primary_key=True)
    type = Column(String(50), nullable=False)

    post_date = Column(
        DateTime,
        nullable=False,
        default=datetime.utcnow() + timedelta(hours=1),
    )
    expiration_date = Column(
        DateTime,
        nullable=False,
        default=datetime.utcnow() + timedelta(weeks=4),
    )
    carrousel = Column(Boolean, nullable=False, default=False)

    languages: List["PostLanguage"] = relationship(
        "PostLanguage", back_populates="post", cascade="all, delete"
    )

    __mapper_args__ = {
        "polymorphic_identity": "post",
        "polymorphic_on": type,
    }


class PostLanguage(Base):
    __tablename__ = "post_language"

    id = Column(Integer, primary_key=True)
    type = Column(String(50), nullable=False)
    lang_code = Column(String(10), nullable=False)
    post_id = Column(
        Integer, ForeignKey("post.id", ondelete="CASCADE"), nullable=False
    )

    subtype = Column(String(50), nullable=False)
    title = Column(String(100), nullable=False)
    body = Column(Text, nullable=False, default="")

    post: "Post" = relationship("Post", back_populates="languages")

    __mapper_args__ = {
        "polymorphic_identity": "post",
        "polymorphic_on": type,
    }

    @hybrid_property
    def subtype_slug(self):
        return slugify(self.subtype)

    @hybrid_property
    def slug(self):
        return slugify(self.title)


class New(Post):
    __tablename__ = "post_new"
    id = Column(
        Integer, ForeignKey("post.id", ondelete="CASCADE"), primary_key=True
    )

    __mapper_args__ = {
        "polymorphic_identity": "new",
    }

    @hybrid_property
    def importance(self):
        # this getter is used when accessing the property of an instance
        now = datetime.utcnow()
        if now < self.post_date:
            return 0
        else:
            return 1 - (
                (now - self.post_date)
                / (self.expiration_date - self.post_date)
            )


class NewLanguage(PostLanguage):
    __tablename__ = "post_new_language"
    id = Column(
        Integer,
        ForeignKey("post_language.id", ondelete="CASCADE"),
        primary_key=True,
    )

    subtitle = Column(String(200))

    __mapper_args__ = {
        "polymorphic_identity": "new",
    }


class Event(Post):
    __tablename__ = "post_event"
    id = Column(
        Integer, ForeignKey("post.id", ondelete="CASCADE"), primary_key=True
    )

    start_datetime = Column(
        DateTime, nullable=False, default=datetime.now() + timedelta(weeks=1)
    )
    end_datetime = Column(
        DateTime, nullable=False, default=datetime.now() + timedelta(weeks=2)
    )

    __mapper_args__ = {
        "polymorphic_identity": "event",
    }

    @hybrid_property
    def importance(self):
        # this getter is used when accessing the property of an instance
        now = datetime.utcnow()
        if now < self.post_date:
            return 0
        elif self.post_date < now < self.start_datetime:
            return (now - self.post_date) / (
                self.start_datetime - self.post_date
            )
        elif self.start_datetime < now < self.end_datetime:
            return 1
        else:  # self.end_datetime < now
            return 1 - (
                (now - self.end_datetime)
                / (self.expiration_date - self.end_datetime)
            )


class EventLanguage(PostLanguage):
    __tablename__ = "post_event_language"
    id = Column(
        Integer,
        ForeignKey("post_language.id", ondelete="CASCADE"),
        primary_key=True,
    )

    location = Column(String(200))
    speaker = Column(String(200))

    __mapper_args__ = {
        "polymorphic_identity": "event",
    }
