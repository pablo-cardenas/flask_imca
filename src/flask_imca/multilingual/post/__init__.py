import os
from datetime import timedelta
from datetime import timezone

import markdown
from flask import Blueprint
from flask import current_app
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import send_from_directory
from flask import url_for
from flask_babel import get_locale
from flask_babel import get_timezone
from flask_babel import Locale
from PIL import Image
from sqlalchemy import select
from werkzeug.datastructures import CombinedMultiDict
from werkzeug.exceptions import abort

from flask_imca.database import db_session
from flask_imca.forms import EventForm
from flask_imca.forms import NewForm
from flask_imca.multilingual.auth import login_required
from flask_imca.multilingual.post.models import Event
from flask_imca.multilingual.post.models import EventLanguage
from flask_imca.multilingual.post.models import New
from flask_imca.multilingual.post.models import NewLanguage
from flask_imca.multilingual.post.models import Post
from flask_imca.multilingual.post.models import PostLanguage

bp = Blueprint("post", __name__, template_folder="templates")


def get_post_language(post_type, post_subtype_slug, slug):
    post_language = [
        pl
        for pl in db_session.execute(
            select(PostLanguage).filter_by(
                lang_code=g.lang_code,
            )
        ).scalars()
        if (
            pl.slug == slug
            and pl.post.type == post_type
            and pl.subtype_slug == post_subtype_slug
        )
    ]
    assert len(post_language) == 1
    return post_language[0]


@bp.route(
    "/post/<post_type>/<post_subtype_slug>/<slug>",
    defaults={"lang_code": "en"},
)
@bp.route(
    "/noticia/<post_type>/<post_subtype_slug>/<slug>",
    defaults={"lang_code": "es"},
)
def detail(post_type, post_subtype_slug, slug):
    post_language = get_post_language(post_type, post_subtype_slug, slug)
    post = post_language.post

    md = markdown.Markdown(extensions=["tables", "fenced_code"])
    html_body = md.convert(post_language.body)

    view_args = {}
    for lang_code in ["es", "en"]:
        post_language = db_session.execute(
            select(PostLanguage).filter_by(post=post, lang_code=lang_code)
        ).scalar_one()
        view_args[lang_code] = {
            "post_type": post.type,
            "post_subtype_slug": post_language.subtype_slug,
            "slug": post_language.slug,
        }

    return render_template(
        "multilingual/post/detail.html",
        html_body=html_body,
        post=post,
        view_args=view_args,
    )


@bp.route(
    "/post/<post_type>/<post_subtype_slug>/<slug>/photo",
    defaults={"lang_code": "en"},
)
@bp.route(
    "/noticia/<post_type>/<post_subtype_slug>/<slug>/photo",
    defaults={"lang_code": "es"},
)
def photo(post_type, post_subtype_slug, slug):
    post_language = get_post_language(post_type, post_subtype_slug, slug)

    post = post_language.post

    return send_from_directory(
        directory=os.path.join(
            current_app.config["MEDIA_FOLDER"],
            "post",
        ),
        path=f"{post.id}.jpg",
        download_name=f"{post_language.slug}.jpg",
    )


@bp.route(
    "/post/<int:id>/update",
    methods=("GET", "POST"),
    defaults={"lang_code": "en"},
)
@bp.route(
    "/noticia/<int:id>/update",
    methods=("GET", "POST"),
    defaults={"lang_code": "es"},
)
@login_required
def update(id):
    if not g.user.is_admin:
        abort(403)

    post = db_session.get(Post, id)
    post_language = post.language(get_locale())

    if request.method == "GET":
        if post.type == "new":
            form = NewForm()
        elif post.type == "event":
            form = EventForm()

        form.post_date.data = (
            post.post_date.replace(tzinfo=timezone.utc)
            .astimezone(get_timezone())
            .replace(tzinfo=None)
        )
        form.expiration_date.data = (
            post.expiration_date.replace(tzinfo=timezone.utc)
            .astimezone(get_timezone())
            .replace(tzinfo=None)
        )

        form.subtype.spanish.data = post.language(Locale("es")).subtype
        form.subtype.english.data = post.language(Locale("en")).subtype

        form.title.spanish.data = post.language(Locale("es")).title
        form.title.english.data = post.language(Locale("en")).title

        form.body.spanish.data = post.language(Locale("es")).body
        form.body.english.data = post.language(Locale("en")).body

        if post.type == "new":
            form.subtitle.spanish.data = post.language(Locale("es")).subtitle
            form.subtitle.english.data = post.language(Locale("en")).subtitle

        elif post.type == "event":
            form.start_datetime.data = (
                post.start_datetime.replace(tzinfo=timezone.utc)
                .astimezone(get_timezone())
                .replace(tzinfo=None)
            )
            form.end_datetime.data = (
                post.end_datetime.replace(tzinfo=timezone.utc)
                .astimezone(get_timezone())
                .replace(tzinfo=None)
            )

            form.speaker.spanish.data = post.language(Locale("es")).speaker
            form.speaker.english.data = post.language(Locale("en")).speaker

            form.location.spanish.data = post.language(Locale("es")).location
            form.location.english.data = post.language(Locale("en")).location

            form.speaker.spanish.data = post.language(Locale("es")).speaker
            form.speaker.english.data = post.language(Locale("en")).speaker

    else:
        if post.type == "new":
            form = NewForm(CombinedMultiDict((request.form, request.files)))
        elif post.type == "event":
            form = EventForm(CombinedMultiDict((request.form, request.files)))

        if form.validate():
            if post.type == "new":
                form = NewForm(
                    CombinedMultiDict(
                        (
                            request.form,
                            request.files,
                        )
                    )
                )
            elif post.type == "event":
                form = EventForm(
                    CombinedMultiDict(
                        (
                            request.form,
                            request.files,
                        )
                    )
                )

            post.post_date = form.post_date.data + timedelta(hours=5)
            post.expiration_date = form.expiration_date.data + timedelta(
                hours=5
            )

            post.language(Locale("en")).subtype = form.subtype.english.data
            post.language(Locale("es")).subtype = form.subtype.spanish.data

            post.language(Locale("en")).title = form.title.english.data
            post.language(Locale("es")).title = form.title.spanish.data

            post.language(Locale("en")).body = form.body.english.data
            post.language(Locale("es")).body = form.body.spanish.data

            if post.type == "new":
                post.language(
                    Locale("en")
                ).subtitle = form.subtitle.english.data
                post.language(
                    Locale("es")
                ).subtitle = form.subtitle.spanish.data

            elif post.type == "event":
                post.start_datetime = form.start_datetime.data + timedelta(
                    hours=5
                )
                post.end_datetime = form.end_datetime.data + timedelta(hours=5)

                post.language(
                    Locale("en")
                ).location = form.location.english.data
                post.language(
                    Locale("es")
                ).location = form.location.spanish.data

                post.language(Locale("en")).speaker = form.speaker.english.data
                post.language(Locale("es")).speaker = form.speaker.spanish.data

            db_session.commit()

            if form.photo.data:
                img = Image.open(form.photo.data).convert("RGB")

                new_size = list(img.size)
                new_size[0] = min(700, new_size[0])
                new_size[1] = int(new_size[1] * new_size[0] / img.size[0])

                new_img = img.resize(new_size, Image.ANTIALIAS)

                photo_path = os.path.join(
                    current_app.config["MEDIA_FOLDER"],
                    "post",
                    f"{post.id}.jpg",
                )
                new_img.save(photo_path, optimize=True, quality=85)

            return redirect(
                url_for(
                    "multilingual.post.detail",
                    post_type=post.type,
                    post_subtype_slug=post.language(
                        Locale(g.lang_code)
                    ).subtype_slug,
                    slug=post_language.slug,
                )
            )

    return render_template(
        "multilingual/post/update.html", post=post, form=form
    )


@bp.route(
    "/post/create/<post_type>", methods=("POST",), defaults={"lang_code": "en"}
)
@bp.route(
    "/noticia/create/<post_type>",
    methods=("POST",),
    defaults={"lang_code": "es"},
)
@login_required
def create(post_type):
    if not g.user.is_admin:
        abort(403)

    if post_type == "event":
        post = Event(
            languages=[
                EventLanguage(
                    lang_code="en",
                    subtype="event",
                    title="Event in English",
                ),
                EventLanguage(
                    lang_code="es",
                    subtype="evento",
                    title="Evento en Español",
                ),
            ],
        )
    elif post_type == "new":
        post = New(
            languages=[
                NewLanguage(
                    lang_code="en",
                    subtype="new",
                    title="New in English",
                ),
                NewLanguage(
                    lang_code="es",
                    subtype="Noticia",
                    title="Noticia en Español",
                ),
            ],
        )

    db_session.add(post)
    db_session.commit()

    return redirect(url_for("multilingual.dashboard.index"))


@bp.route("/post/delete/<id>", methods=("POST",), defaults={"lang_code": "en"})
@bp.route(
    "/noticia/delete/<id>", methods=("POST",), defaults={"lang_code": "es"}
)
@login_required
def delete(id):
    if not g.user.is_admin:
        abort(403)

    post = db_session.get(Post, id)

    # Remove photo
    try:
        photo_path = os.path.join(
            current_app.config["MEDIA_FOLDER"],
            "post",
            f"{post.id}.jpg",
        )
        os.remove(photo_path)
    except OSError:
        pass

    db_session.delete(post)
    db_session.commit()

    return redirect(url_for("multilingual.dashboard.index"))
