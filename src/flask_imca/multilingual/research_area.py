import os

import markdown
from babel import Locale
from flask import Blueprint
from flask import current_app
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import send_from_directory
from flask import url_for
from flask_babel import get_locale
from PIL import Image
from pybtex.database import parse_string
from sqlalchemy import select
from werkzeug.datastructures import CombinedMultiDict
from werkzeug.exceptions import abort

from flask_imca.database import db_session
from flask_imca.forms import ResearchAreaForm
from flask_imca.multilingual.auth import login_required
from flask_imca.multilingual.models import ResearchArea
from flask_imca.multilingual.models import ResearchAreaLanguage

bp = Blueprint("research_area", __name__)


@bp.route("/areas-de-investigacion", defaults={"lang_code": "es"})
@bp.route("/research-areas", defaults={"lang_code": "en"})
def index():
    research_areas = db_session.execute(select(ResearchArea)).scalars()
    return render_template(
        "multilingual/research_area/index.html", research_areas=research_areas
    )


@bp.route("/area-de-investigacion/<slug>", defaults={"lang_code": "es"})
@bp.route("/research-area/<slug>", defaults={"lang_code": "en"})
def detail(slug):
    research_area_language = [
        ra
        for ra in db_session.execute(
            select(ResearchAreaLanguage).filter_by(lang_code=g.lang_code)
        ).scalars()
        if ra.slug == slug
    ]
    assert len(research_area_language) == 1
    research_area_language = research_area_language[0]
    research_area = research_area_language.research_area
    research_areas = db_session.execute(select(ResearchArea)).scalars().all()

    md = markdown.Markdown(extensions=["tables"])
    html_summary = md.convert(research_area_language.summary)

    publications = parse_string(research_area.publications, "bibtex")

    view_args = {}
    for lang_code in ["es", "en"]:
        locale = Locale.parse(lang_code)
        view_args[lang_code] = {"slug": research_area.language(locale).slug}

    return render_template(
        "multilingual/research_area/detail.html",
        research_area=research_area,
        html_summary=html_summary,
        research_areas=research_areas,
        publications=publications,
        view_args=view_args,
    )


@bp.route(
    "/area-de-investigacion/<int:id>/update",
    methods=("GET", "POST"),
    defaults={"lang_code": "es"},
)
@bp.route(
    "/research-area/<int:id>/update",
    methods=("GET", "POST"),
    defaults={"lang_code": "en"},
)
@login_required
def update(id):
    research_area = db_session.get(ResearchArea, id)

    if g.user != research_area.user and not g.user.is_admin:
        abort(403)

    if request.method == "GET":
        form = ResearchAreaForm()
        form.summary.english.data = research_area.language(
            Locale("en")
        ).summary
        form.summary.spanish.data = research_area.language(
            Locale("es")
        ).summary
        form.publications.data = research_area.publications
    else:
        form = ResearchAreaForm(
            CombinedMultiDict((request.form, request.files))
        )

        if form.validate():
            research_area.language(
                Locale("en")
            ).summary = form.summary.english.data
            research_area.language(
                Locale("es")
            ).summary = form.summary.spanish.data
            research_area.publications = form.publications.data
            db_session.commit()

            if form.photo.data:
                img = Image.open(form.photo.data).convert("RGB")

                new_size = list(img.size)
                new_size[0] = min(400, new_size[0])
                new_size[1] = int(new_size[1] * new_size[0] / img.size[0])

                new_img = img.resize(new_size, Image.ANTIALIAS)

                photo_path = os.path.join(
                    current_app.config["MEDIA_FOLDER"],
                    "research-area",
                    f"{research_area.id}.jpg",
                )
                new_img.save(photo_path, optimize=True, quality=85)

            research_area_language = research_area.language(get_locale())
            return redirect(
                url_for(
                    "multilingual.research_area.detail",
                    slug=research_area_language.slug,
                )
            )

    return render_template(
        "multilingual/research_area/update.html",
        research_area=research_area,
        form=form,
    )


@bp.route("/research-area/<slug>/photo")
def photo(slug):
    research_area_language = [
        ra
        for ra in db_session.execute(
            select(ResearchAreaLanguage).filter_by(lang_code=g.lang_code)
        ).scalars()
        if ra.slug == slug
    ]
    assert len(research_area_language) == 1
    research_area_language = research_area_language[0]
    research_area = research_area_language.research_area

    return send_from_directory(
        directory=os.path.join(
            current_app.config["MEDIA_FOLDER"],
            "research-area",
        ),
        path=f"{research_area.id}.jpg",
        download_name=f"{slug}.jpg",
    )
