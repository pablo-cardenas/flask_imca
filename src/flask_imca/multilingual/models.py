from typing import List

from slugify import slugify  # type: ignore
from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Date
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy import text
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import declarative_mixin
from sqlalchemy.orm import relationship

from flask_imca.models import Base


@declarative_mixin
class LanguageMixin:
    def language(self, locale):
        for language in self.languages:
            if language.lang_code == locale.language:
                return language


class User(LanguageMixin, Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    email: str = Column(String(50), nullable=False, unique=True)
    lastname1: str = Column(String(25), nullable=False)
    lastname2: str = Column(String(25), nullable=False)
    firstname: str = Column(String(50), nullable=False)
    is_admin: bool = Column(
        Boolean, nullable=False, default=0, server_default=text("0")
    )

    languages: List["UserLanguage"] = relationship(
        "UserLanguage", back_populates="user"
    )
    researchers: "Researcher" = relationship(
        "Researcher", back_populates="user"
    )
    graduated_students: "GraduatedStudent" = relationship(
        "GraduatedStudent", back_populates="user"
    )

    @hybrid_property
    def name(self) -> str:
        return f"{self.firstname} {self.lastname1} {self.lastname2}"

    @hybrid_property
    def slug(self) -> str:
        return slugify(self.name)  # type: ignore


class UserLanguage(Base):
    __tablename__ = "user_language"

    id = Column(Integer, primary_key=True)
    lang_code: str = Column(String(10), nullable=False)
    user_id: int = Column(
        Integer,
        ForeignKey("user.id", ondelete="CASCADE"),
        nullable=False,
    )

    summary: str = Column(Text, nullable=False, default="", server_default="")

    user: "User" = relationship("User", back_populates="languages")


class GraduatedStudent(LanguageMixin, Base):
    __tablename__ = "graduated_student"
    id = Column(Integer, primary_key=True)
    user_id: int = Column(
        Integer, ForeignKey("user.id", ondelete="CASCADE"), nullable=False
    )
    defense_date = Column(Date, nullable=False)
    program_id: int = Column(Integer, ForeignKey("program.id"), nullable=False)
    advisor: str = Column(String(100), nullable=False)
    external_advisor: str = Column(String(100))
    title: str = Column(String(100), nullable=False)
    supervisor1: str = Column(String(100), nullable=False)
    supervisor2: str = Column(String(100), nullable=False)

    user: "User" = relationship("User", back_populates="graduated_students")
    program: "Program" = relationship("Program")

    @hybrid_property
    def name(self) -> str:
        return self.user.name

    @hybrid_property
    def slug(self) -> str:
        return self.user.slug


class CurrentStudent(User):
    __tablename__ = "current_student"
    id = Column(
        Integer, ForeignKey("user.id", ondelete="CASCADE"), primary_key=True
    )

    __mapper_args__ = {
        "polymorphic_identity": "current_student",
    }


class Researcher(LanguageMixin, Base):
    __tablename__ = "researcher"

    id = Column(Integer, primary_key=True)
    user_id: int = Column(
        Integer,
        ForeignKey("user.id", ondelete="CASCADE"),
        nullable=False,
        unique=True,
    )
    is_associated: int = Column(Integer, nullable=False)

    publications: str = Column(
        Text, nullable=False, default="", server_default=""
    )
    languages: List["ResearcherLanguage"] = relationship(
        "ResearcherLanguage", back_populates="researcher"
    )

    user: "User" = relationship("User", back_populates="researchers")

    @hybrid_property
    def name(self) -> str:
        firstname1 = self.user.firstname.split(" ")[0]
        return f"Prof. {firstname1} {self.user.lastname1}"


class ResearcherLanguage(Base):
    __tablename__ = "researcher_language"
    id = Column(Integer, primary_key=True)
    lang_code: str = Column(String(10), nullable=False)
    researcher_id: int = Column(
        Integer,
        ForeignKey("researcher.id", ondelete="CASCADE"),
        nullable=False,
    )

    field: str = Column(
        String(50), nullable=False, default="", server_default=""
    )

    researcher: "Researcher" = relationship(
        "Researcher", back_populates="languages"
    )


class Staff(LanguageMixin, Base):
    __tablename__ = "staff"

    id = Column(Integer, primary_key=True)
    user_id: int = Column(
        Integer, ForeignKey("user.id", ondelete="CASCADE"), nullable=False
    )

    languages: List["StaffLanguage"] = relationship(
        "StaffLanguage", back_populates="staff"
    )
    user: "User" = relationship("User")


class StaffLanguage(Base):
    __tablename__ = "staff_language"
    id = Column(Integer, primary_key=True)
    lang_code: str = Column(String(10), nullable=False)
    staff_id: int = Column(
        Integer, ForeignKey("staff.id", ondelete="CASCADE"), nullable=False
    )

    staff: "Staff" = relationship("Staff", back_populates="languages")


class ResearchArea(LanguageMixin, Base):
    __tablename__ = "research_area"
    id = Column(Integer, primary_key=True)

    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)

    user: "User" = relationship("User")
    publications = Column(Text, nullable=False, default="", server_default="")

    languages: List["ResearchAreaLanguage"] = relationship(
        "ResearchAreaLanguage", back_populates="research_area"
    )


class ResearchAreaLanguage(Base):
    __tablename__ = "research_area_language"
    id = Column(Integer, primary_key=True)
    lang_code: str = Column(String(50), nullable=False)
    research_area_id: int = Column(
        Integer, ForeignKey("research_area.id"), nullable=False
    )

    name: str = Column(String(50), nullable=False)
    summary: str = Column(Text, nullable=False)

    research_area: "ResearchArea" = relationship(
        "ResearchArea", back_populates="languages"
    )

    @hybrid_property
    def slug(self) -> str:
        return slugify(self.name)


class FlatPage(LanguageMixin, Base):
    __tablename__ = "flatpage"
    id = Column(Integer, primary_key=True)

    languages: List["FlatPageLanguage"] = relationship(
        "FlatPageLanguage", back_populates="flatpage"
    )


class FlatPageLanguage(Base):
    __tablename__ = "flatpage_language"
    id = Column(Integer, primary_key=True)
    lang_code: str = Column(String(50), nullable=False)
    flatpage_id: int = Column(
        Integer, ForeignKey("flatpage.id"), nullable=False
    )

    title: str = Column(String(50), nullable=False)
    content: str = Column(Text, nullable=False)

    flatpage: "FlatPage" = relationship("FlatPage", back_populates="languages")

    @hybrid_property
    def slug(self) -> str:
        return slugify(self.title)


class Program(LanguageMixin, Base):
    __tablename__ = "program"
    id = Column(Integer, primary_key=True)
    is_active: bool = Column(Boolean, nullable=False)
    degree: str = Column(String(50))

    languages: List["ProgramLanguage"] = relationship(
        "ProgramLanguage", back_populates="program"
    )


class ProgramLanguage(Base):
    __tablename__ = "program_language"
    id = Column(Integer, primary_key=True)
    lang_code: str = Column(String(50), nullable=False)
    program_id: int = Column(Integer, ForeignKey("program.id"), nullable=False)

    degree: str = Column(String(50))
    title: str = Column(String(50))
    body: str = Column(Text, nullable=False, default="", server_default="")

    program: "Program" = relationship("Program", back_populates="languages")

    @hybrid_property
    def name(self) -> str:
        return f"{self.degree} {self.title}"

    @hybrid_property
    def slug(self) -> str:
        return slugify(self.name)
