import locale
import os

import markdown
from flask import Blueprint
from flask import current_app
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import send_from_directory
from flask import session
from flask import url_for
from flask_babel import get_locale
from flask_babel import Locale
from PIL import Image
from pybtex.database import parse_string
from sqlalchemy import select
from werkzeug.datastructures import CombinedMultiDict
from werkzeug.exceptions import abort

from flask_imca.database import db_session
from flask_imca.forms import GraduatedStudentForm
from flask_imca.forms import ResearcherForm
from flask_imca.multilingual.auth import login_required
from flask_imca.multilingual.models import GraduatedStudent
from flask_imca.multilingual.models import Researcher
from flask_imca.multilingual.models import ResearcherLanguage
from flask_imca.multilingual.models import Staff
from flask_imca.multilingual.models import User
from flask_imca.multilingual.models import UserLanguage

bp = Blueprint("people", __name__, template_folder="templates")


def get_user(id=None, slug=None, check_same_logged=True):
    user = None
    if id is not None:
        user = db_session.get(User, id)
    elif slug is not None:
        users = [
            u
            for u in db_session.execute(select(User)).scalars()
            if u.slug == slug
        ]
        assert len(users) == 1
        user = users[0]

    if user is None:
        return abort(404)

    if check_same_logged and g.user and g.user != user and not g.user.is_admin:
        return abort(403)

    return user


def process_photo(img, target_size):
    ratio_to_target = (
        target_size[0] / img.size[0],
        target_size[1] / img.size[1],
    )

    if ratio_to_target[0] > ratio_to_target[1]:
        region = [
            0,
            (img.size[1] - target_size[1] / ratio_to_target[0]) / 2,
            img.size[0],
            (img.size[1] + target_size[1] / ratio_to_target[0]) / 2,
        ]
    else:
        region = [
            (img.size[0] - target_size[0] / ratio_to_target[1]) / 2,
            0,
            (img.size[0] + target_size[0] / ratio_to_target[1]) / 2,
            img.size[1],
        ]

    if max(*ratio_to_target) > 1:
        new_img = img.crop([int(x) for x in region])
    else:
        new_img = img.transform(
            target_size,
            Image.EXTENT,
            data=region,
        )

    return new_img


@bp.route("/personas", defaults={"lang_code": "es"})
@bp.route("/people", defaults={"lang_code": "en"})
def index():
    locale.setlocale(locale.LC_COLLATE, "en_US.UTF-8")
    researchers = db_session.query(Researcher).all()
    researchers.sort(key=lambda x: locale.strxfrm(x.user.firstname))

    graduated_students = db_session.query(GraduatedStudent).all()
    graduated_students.sort(key=lambda x: x.defense_date, reverse=True)

    administrative_staff = db_session.query(Staff).all()
    administrative_staff.sort(key=lambda x: x.user.firstname)

    master_graduated_students = list(
        filter(lambda x: "master" in x.program.degree, graduated_students)
    )
    doctorate_graduated_students = list(
        filter(lambda x: "doctorate" in x.program.degree, graduated_students)
    )

    return render_template(
        "multilingual/people/index.html",
        researchers=researchers,
        graduated_students=graduated_students,
        administrative_staff=administrative_staff,
        master_graduated_students=master_graduated_students,
        doctorate_graduated_students=doctorate_graduated_students,
        current_section=None,
    )


@bp.route("/investigador/<slug>", defaults={"lang_code": "es"})
@bp.route("/researcher/<slug>", defaults={"lang_code": "en"})
def researcher_detail(slug):
    has_recaptcha = "recaptcha" in session
    session.pop("recaptcha", None)

    user = get_user(slug=slug, check_same_logged=False)
    user_language = db_session.execute(
        select(UserLanguage).filter_by(user=user, lang_code=g.lang_code)
    ).scalar_one()
    researcher = db_session.execute(
        select(Researcher).filter_by(user=user)
    ).scalar_one()
    researcher_language = db_session.execute(
        select(ResearcherLanguage).filter_by(
            researcher=researcher, lang_code=g.lang_code
        )
    ).scalar_one()

    md = markdown.Markdown(extensions=["extra"])
    html_summary = md.convert(user_language.summary)

    # Get the current_section
    current_section = None
    if researcher.is_associated:
        current_section = "associated_researchers"
    else:
        current_section = "scientific_staff"

    publications = parse_string(researcher.publications, "bibtex")
    return render_template(
        "multilingual/people/researcher_detail.html",
        has_recaptcha=has_recaptcha,
        current_section=current_section,
        user=user,
        researcher=researcher,
        researcher_language=researcher_language,
        publications=publications,
        html_summary=html_summary,
        locale=get_locale(),
    )


@bp.route("/alumno-graduado/<slug>", defaults={"lang_code": "es"})
@bp.route("/graduated-student/<slug>", defaults={"lang_code": "en"})
def graduated_student_detail(slug):
    has_recaptcha = "recaptcha" in session
    session.pop("recaptcha", None)

    user = get_user(slug=slug, check_same_logged=False)
    user_language = db_session.execute(
        select(UserLanguage).filter_by(user=user, lang_code=g.lang_code)
    ).scalar_one()
    graduated_students = db_session.execute(
        select(GraduatedStudent).filter_by(user=user)
    ).scalars()

    md = markdown.Markdown(extensions=["extra"])
    html_summary = md.convert(user_language.summary)

    return render_template(
        "multilingual/people/graduated_student_detail.html",
        has_recaptcha=has_recaptcha,
        current_section="graduated_students",
        user=user,
        graduated_students=graduated_students,
        html_summary=html_summary,
        locale=get_locale(),
    )


@bp.route("/personal-administrativo/<slug>", defaults={"lang_code": "es"})
@bp.route("/administrative-staff/<slug>", defaults={"lang_code": "en"})
def staff_detail(slug):
    has_recaptcha = "recaptcha" in session
    session.pop("recaptcha", None)

    user = get_user(slug=slug, check_same_logged=False)
    user_language = db_session.execute(
        select(UserLanguage).filter_by(user=user, lang_code=g.lang_code)
    ).scalar_one()

    md = markdown.Markdown(extensions=["extra"])
    html_summary = md.convert(user_language.summary)

    return render_template(
        "multilingual/people/staff_detail.html",
        has_recaptcha=has_recaptcha,
        current_section="administrative_staff",
        user=user,
        html_summary=html_summary,
        locale=get_locale(),
    )


@bp.route(
    "/investigador/<int:id>/actualizar",
    methods=("GET", "POST"),
    defaults={"lang_code": "es"},
)
@bp.route(
    "/researcher/<int:id>/update",
    methods=("GET", "POST"),
    defaults={"lang_code": "en"},
)
@login_required
def researcher_update(id):
    user = get_user(id)
    researcher = db_session.execute(
        select(Researcher).filter_by(user=user)
    ).scalar_one()

    if request.method == "GET":
        form = ResearcherForm()
        form.summary.english.data = user.language(Locale("en")).summary
        form.summary.spanish.data = user.language(Locale("es")).summary
        form.field.english.data = researcher.language(Locale("en")).field
        form.field.spanish.data = researcher.language(Locale("es")).field

        form.publications.data = researcher.publications
    else:
        form = ResearcherForm(CombinedMultiDict((request.form, request.files)))

        if form.validate():
            researcher.publications = form.publications.data
            user.language(Locale("en")).summary = form.summary.english.data
            user.language(Locale("es")).summary = form.summary.spanish.data

            researcher.language(Locale("en")).field = form.field.english.data
            researcher.language(Locale("es")).field = form.field.spanish.data

            db_session.commit()

            if form.photo.data:
                img = Image.open(form.photo.data).convert("RGB")
                new_img = process_photo(img, (225, 300))

                photo_path = os.path.join(
                    current_app.config["MEDIA_FOLDER"],
                    "teacher",
                    f"{user.id}.jpg",
                )
                new_img.save(photo_path, optimize=True, quality=85)

            return redirect(
                url_for(
                    "multilingual.people.researcher_detail", slug=user.slug
                )
            )

    return render_template(
        "multilingual/people/update.html", teacher=user, form=form
    )


@bp.route("/graduated-student/<int:id>/update", methods=("GET", "POST"))
@login_required
def graduated_student_update(id):
    user = get_user(id)

    if request.method == "GET":
        form = GraduatedStudentForm()
        form.summary.english.data = user.language(Locale("en")).summary
        form.summary.spanish.data = user.language(Locale("es")).summary
    else:
        form = GraduatedStudentForm(
            CombinedMultiDict((request.form, request.files))
        )

        if form.validate():
            user.language(Locale("en")).summary = form.summary.english.data
            user.language(Locale("es")).summary = form.summary.spanish.data
            db_session.commit()

            if form.photo.data:
                img = Image.open(form.photo.data).convert("RGB")
                new_img = process_photo(img, (225, 300))

                photo_path = os.path.join(
                    current_app.config["MEDIA_FOLDER"],
                    "teacher",
                    f"{user.id}.jpg",
                )
                new_img.save(photo_path, optimize=True, quality=85)

            return redirect(
                url_for(
                    "multilingual.people.graduated_student_detail",
                    slug=user.slug,
                )
            )

    return render_template("multilingual/people/update.html", form=form)


@bp.route("/people/<slug>/photo")
def photo(slug):
    teacher = get_user(slug=slug, check_same_logged=False)

    return send_from_directory(
        directory=os.path.join(
            current_app.config["MEDIA_FOLDER"],
            "teacher",
        ),
        path=f"{teacher.id}.jpg",
        download_name=f"{teacher.slug}.jpg",
    )
