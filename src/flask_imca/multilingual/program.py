import markdown
from babel import Locale
from flask import Blueprint
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from flask_babel import get_locale
from sqlalchemy import select
from werkzeug.datastructures import CombinedMultiDict
from werkzeug.exceptions import abort

from flask_imca.database import db_session
from flask_imca.forms import ProgramForm
from flask_imca.multilingual.auth import login_required
from flask_imca.multilingual.models import GraduatedStudent
from flask_imca.multilingual.models import Program
from flask_imca.multilingual.models import ProgramLanguage

bp = Blueprint("program", __name__)


@bp.route("/program/<slug>", defaults={"lang_code": "en"})
@bp.route("/programa/<slug>", defaults={"lang_code": "es"})
def detail(slug):
    program_language = [
        p
        for p in db_session.execute(
            select(ProgramLanguage).filter_by(lang_code=g.lang_code)
        ).scalars()
        if p.slug == slug
    ]
    assert len(program_language) == 1
    program_language = program_language[0]
    program = program_language.program

    programs = (
        db_session.execute(select(Program).filter_by(is_active=True))
        .scalars()
        .all()
    )

    graduated_students = db_session.execute(
        select(GraduatedStudent).filter_by(program=program)
    ).scalars()

    md = markdown.Markdown(extensions=["tables", "fenced_code"])
    html_body = md.convert(program_language.body)

    view_args = {}
    for lang_code in ["es", "en"]:
        locale = Locale.parse(lang_code)
        view_args[lang_code] = {"slug": program.language(locale).slug}

    return render_template(
        "multilingual/program/detail.html",
        program=program,
        programs=programs,
        graduated_students=graduated_students,
        html_body=html_body,
        view_args=view_args,
    )


@bp.route("/program/<int:id>/update", methods=("GET", "POST"))
@login_required
def update(id):
    program = db_session.get(Program, id)

    if not g.user.is_admin:
        abort(403)

    if request.method == "GET":
        form = ProgramForm()
        form.body.english.data = program.language(Locale("en")).body
        form.body.spanish.data = program.language(Locale("es")).body
    else:
        form = ProgramForm(CombinedMultiDict((request.form, request.files)))

        if form.validate():
            program.language(Locale("en")).body = form.body.english.data
            program.language(Locale("es")).body = form.body.spanish.data
            db_session.commit()

            program_language = program.language(get_locale())
            return redirect(
                url_for(
                    "multilingual.program.detail", slug=program_language.slug
                )
            )

    return render_template(
        "multilingual/program/update.html", program=program, form=form
    )
