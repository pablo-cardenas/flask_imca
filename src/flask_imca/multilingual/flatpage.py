import markdown
from babel import Locale
from flask import Blueprint
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from sqlalchemy import select
from sqlalchemy.orm.exc import NoResultFound
from werkzeug.datastructures import CombinedMultiDict
from werkzeug.exceptions import abort

from flask_imca.database import db_session
from flask_imca.forms import FlatPageForm
from flask_imca.multilingual.auth import login_required
from flask_imca.multilingual.models import FlatPageLanguage

bp = Blueprint("flatpage", __name__, url_prefix="/page")


@bp.route("/<slug>")
def flatpage(slug):
    try:
        fpages = [
            f
            for f in db_session.execute(
                select(FlatPageLanguage).where(
                    FlatPageLanguage.lang_code == g.lang_code
                )
            ).scalars()
            if slug == f.slug
        ]
        assert len(fpages) == 1
        fpage = fpages[0].flatpage

    except NoResultFound:
        abort(404)

    view_args = {}
    for lang_code in ["es", "en"]:
        locale = Locale.parse(lang_code)
        view_args[lang_code] = {
            "slug": fpage.language(locale).slug,
        }

    md = markdown.Markdown(extensions=["toc", "tables", "fenced_code"])
    html = md.convert(fpage.language(Locale.parse(g.lang_code)).content)

    return render_template(
        "multilingual/flatpage/flatpage.html",
        flatpage=fpage,
        html=html,
        toc=md.toc,
        view_args=view_args,
    )


@bp.route("/<slug>/update", methods=("GET", "POST"))
@login_required
def update(slug):
    if not g.user.is_admin:
        abort(403)

    try:
        fpage_languages = [
            f
            for f in db_session.execute(
                select(FlatPageLanguage).where(
                    FlatPageLanguage.lang_code == g.lang_code
                )
            ).scalars()
            if slug == f.slug
        ]
        assert len(fpage_languages) == 1
        fpage_language = fpage_languages[0]
        fpage = fpage_language.flatpage
    except NoResultFound:
        abort(404)

    if request.method == "GET":
        form = FlatPageForm()
        form.content.english.data = fpage.language(Locale("en")).content
        form.content.spanish.data = fpage.language(Locale("es")).content
    else:
        form = FlatPageForm(CombinedMultiDict((request.form, request.files)))

        if form.validate():
            fpage.language(Locale("en")).content = form.content.english.data
            fpage.language(Locale("es")).content = form.content.spanish.data
            db_session.commit()

            return redirect(
                url_for(
                    "multilingual.flatpage.flatpage", slug=fpage_language.slug
                )
            )

    return render_template(
        "multilingual/flatpage/update.html", flatpage=fpage, form=form
    )
