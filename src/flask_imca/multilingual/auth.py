import functools

from authlib.integrations.flask_client import OAuth
from flask import Blueprint
from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound
from werkzeug.exceptions import abort

from flask_imca.database import db_session
from flask_imca.multilingual.models import User

bp = Blueprint("auth", __name__, url_prefix="/auth")

oauth = OAuth()
oauth.register(
    name="google",
    server_metadata_url=(
        "https://accounts.google.com/.well-known/openid-configuration"
    ),
    client_kwargs={"scope": "openid email profile"},
)


@bp.route("/login")
def login():
    return render_template("multilingual/auth/login.html")


@bp.route("/login/google")
def login_google():
    redirect_uri = url_for("multilingual.auth.authorize", _external=True)

    if "next" in request.args:
        session["next"] = request.args["next"]

    return oauth.google.authorize_redirect(redirect_uri)


@bp.route("/authorize")
def authorize():
    token = oauth.google.authorize_access_token()

    user_email = token["userinfo"]["email"]

    url_next = session.pop("next", url_for("index"))

    try:
        user = db_session.execute(
            select(User).filter_by(email=user_email)
        ).scalar_one()
    except NoResultFound:
        flash(
            f'The email "{user_email}" is not registered.'
            " Check your email or contact with pablo.cardenas@imca.edu.pe."
        )
        return redirect(url_for("multilingual.auth.login"))
    else:
        session["user_id"] = user.id

    return redirect(url_next)


@bp.route("/logout")
def logout():
    url_next = request.args.get("next", url_for("index"))
    session.pop("user_id", None)
    return redirect(url_next)


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get("user_id")

    if user_id is None:
        g.user = None
    else:
        g.user = db_session.get(User, user_id)


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if "user_id" not in session:
            return redirect(
                url_for("multilingual.auth.login", next=request.path)
            )

        return view(**kwargs)

    return wrapped_view


def get_user(id=None, slug=None, check_same_logged=True, check_admin=True):
    user = None
    if id is not None:
        user = db_session.get(User, id)
    elif slug is not None:
        users = [
            u
            for u in db_session.execute(select(User)).scalars()
            if u.slug == slug
        ]
        if len(users) == 1:
            user = users[0]

    if user is None:
        return abort(404)

    if check_same_logged and g.user != user:
        return abort(403)

    if check_admin and not g.user.is_admin:
        return abort(403)

    return user
