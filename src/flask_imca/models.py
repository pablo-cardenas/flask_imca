from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import declarative_base


class Base_:
    __allow_unmapped__ = True


Base = declarative_base(cls=Base_)


class FileUploaded(Base):
    __tablename__ = "file_uploaded"
    id = Column(Integer, primary_key=True)

    filename = Column(String(100))
    mimetype = Column(String(50))
