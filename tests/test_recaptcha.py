def test_recaptcha(client):
    response = client.get(
        "/en/recaptcha?next=%2Fen%2Fresearcher%2Fgonzalo-panizo-garcia"
    )
    assert response.status_code == 200
