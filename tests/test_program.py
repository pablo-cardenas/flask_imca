from flask_babel import Locale

from flask_imca.database import db_session
from flask_imca.multilingual.models import Program


def test_detail(client, auth):
    response = client.get("/en/program/master-in-applied-mathematics")
    assert response.status_code == 200
    assert b">Edit<" not in response.data

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/program/master-in-applied-mathematics")
        assert response.status_code == 200
        assert b">Edit<" not in response.data

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/program/master-in-applied-mathematics")
        assert response.status_code == 200
        assert b">Edit<" in response.data


def test_update_login_required(client, auth):
    response = client.get("/en/program/1/update")
    assert response.status_code == 302
    assert (
        response.headers["Location"]
        == "/en/auth/login?next=%2Fen%2Fprogram%2F1%2Fupdate"
    )

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/program/1/update")
        assert response.status_code == 403

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/program/1/update")
        assert response.status_code == 200


def test_update(client, auth, app):
    auth.login("admin@imca.edu.pe")

    response = client.get("/en/program/1/update")
    response = client.post(
        "/en/program/1/update",
        data={
            "body-english": "body updated",
            "body-spanish": "contenido actualizado",
        },
    )
    assert response.status_code == 302
    assert (
        response.headers["Location"]
        == "/en/program/master-in-applied-mathematics"
    )

    with app.app_context():
        program = db_session.get(Program, 1)
        assert program.language(Locale("en")).body == "body updated"
