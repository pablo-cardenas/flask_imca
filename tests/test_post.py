from flask_babel import Locale

from flask_imca.database import db_session
from flask_imca.multilingual.post.models import Post


def test_detail(client, auth):
    response = client.get("/en/post/new/announcement/title-in-english")
    assert response.status_code == 200
    assert b">Edit<" not in response.data
    assert b"title in english" in response.data
    assert b"subtitle in english" in response.data

    response = client.get("/en/post/event/talk/event-title-in-english")
    assert response.status_code == 200
    assert b">Edit<" not in response.data
    assert b"event title in english" in response.data
    assert b"body in english" in response.data

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/post/new/announcement/title-in-english")
        assert response.status_code == 200
        assert b">Edit<" not in response.data

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/post/new/announcement/title-in-english")
        assert response.status_code == 200
        assert b">Edit<" in response.data


def test_update_new(client, auth):
    response = client.get("/en/post/1/update")
    assert response.status_code == 302
    assert (
        response.headers["Location"]
        == "/en/auth/login?next=%2Fen%2Fpost%2F1%2Fupdate"
    )

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/post/1/update")
        assert response.status_code == 403

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/post/1/update")
        assert response.status_code == 200

    # TODO: test method post


def test_update_event(client, auth):
    response = client.get("/en/post/2/update")
    assert response.status_code == 302
    assert (
        response.headers["Location"]
        == "/en/auth/login?next=%2Fen%2Fpost%2F2%2Fupdate"
    )

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/post/2/update")
        assert response.status_code == 403

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/post/2/update")
        assert response.status_code == 200

    # TODO: test method post


def test_create(client, auth):
    response = client.post("/en/post/create/event")
    assert response.status_code == 302
    assert (
        response.headers["Location"]
        == "/en/auth/login?next=%2Fen%2Fpost%2Fcreate%2Fevent"
    )

    auth.login("test@imca.edu.pe")
    with client:
        response = client.post("/en/post/create/event")
        assert response.status_code == 403

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.post("/en/post/create/event")
        assert response.status_code == 302
        assert response.headers["Location"] == "/en/dashboard"

        post = db_session.get(Post, 3)
        assert post.type == "event"
        assert post.language(Locale("en")).subtype == "event"
        assert post.language(Locale("en")).title == "Event in English"


def test_delete(client, auth, app):
    response = client.post("/en/post/delete/1")
    assert response.status_code == 302
    assert (
        response.headers["Location"]
        == "/en/auth/login?next=%2Fen%2Fpost%2Fdelete%2F1"
    )

    auth.login("test@imca.edu.pe")
    with client:
        response = client.post("/en/post/delete/1")
        assert response.status_code == 403

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.post("/en/post/delete/1")
        assert response.status_code == 302
        assert response.headers["Location"] == "/en/dashboard"

        assert db_session.get(Post, 1) is None


def test_accent_event(client, auth):
    auth.login("admin@imca.edu.pe")
    with client:
        response = client.post(
            "/en/post/1/update",
            data={
                "post_date": "2022-03-21T15:39",
                "expiration_date": "2022-04-18T14:39",
                "subtype-english": "Article",
                "subtype-spanish": "Artículo",
                "title-english": "Article in english",
                "title-spanish": "Artículo en español",
                "body-english": "Body in english",
                "body-spanish": "Cuerpo en español",
                "subtitle-english": "Subtitle in english",
                "subtitle-spanish": "Subtítulo en español",
            },
        )
        assert response.status_code == 302
        assert (
            response.headers["Location"]
            == "/en/post/new/article/article-in-english"
        )

    response = client.get("/en/post/new/article/article-in-english")
    assert response.status_code == 200

    response = client.get("/es/noticia/new/articulo/articulo-en-espanol")
    assert response.status_code == 200
