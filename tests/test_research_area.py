from pybtex.database import parse_string

from flask_imca.database import db_session
from flask_imca.multilingual.models import ResearchArea


def test_detail(client, auth):
    response = client.get("/en/research-area/probability")
    assert response.status_code == 200
    assert b">Edit<" not in response.data

    auth.login("other@imca.edu.pe")
    with client:
        response = client.get("/en/research-area/probability")
        assert response.status_code == 200
        assert b">Edit<" not in response.data

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/research-area/probability")
        assert response.status_code == 200
        assert b">Edit<" in response.data

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/research-area/probability")
        assert response.status_code == 200
        assert b">Edit<" in response.data


def test_update_login_required(client, auth):
    response = client.get("/en/research-area/1/update")
    assert response.status_code == 302
    assert (
        response.headers["Location"]
        == "/en/auth/login?next=%2Fen%2Fresearch-area%2F1%2Fupdate"
    )

    auth.login("other@imca.edu.pe")
    with client:
        response = client.get("/en/research-area/1/update")
        assert response.status_code == 403

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/research-area/1/update")
        assert response.status_code == 200

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/research-area/1/update")
        assert response.status_code == 200


def test_update(client, auth, app):
    auth.login("test@imca.edu.pe")

    assert client.get("/en/research-area/1/update").status_code == 200
    response = client.post(
        "/en/research-area/1/update",
        data={
            "summary-english": "updated summary",
            "summary-spanish": "resumen actualizado",
            "publications": """
                        @article{greenwade93,
                            author  = "George D. Greenwade",
                            title   = "{CTAN}",
                            year    = "2020",
                            journal = "TUGBoat",
                            volume  = "14",
                            number  = "3",
                            pages   = "342--351"
                        }
                    """,
        },
    )
    assert response.status_code == 302
    assert response.headers["Location"] == "/en/research-area/probability"

    with app.app_context():
        research_area = db_session.get(ResearchArea, 1)
        publications = parse_string(research_area.publications, "bibtex")
        assert publications.entries["greenwade93"].fields["year"] == "2020"
