import tempfile
from datetime import date
from datetime import datetime
from datetime import timedelta

import pytest

from flask_imca import create_app
from flask_imca.database import db_session
from flask_imca.database import init_db
from flask_imca.multilingual.models import FlatPage
from flask_imca.multilingual.models import FlatPageLanguage
from flask_imca.multilingual.models import GraduatedStudent
from flask_imca.multilingual.models import Program
from flask_imca.multilingual.models import ProgramLanguage
from flask_imca.multilingual.models import ResearchArea
from flask_imca.multilingual.models import ResearchAreaLanguage
from flask_imca.multilingual.models import Researcher
from flask_imca.multilingual.models import ResearcherLanguage
from flask_imca.multilingual.models import User
from flask_imca.multilingual.models import UserLanguage
from flask_imca.multilingual.post.models import Event
from flask_imca.multilingual.post.models import EventLanguage
from flask_imca.multilingual.post.models import New
from flask_imca.multilingual.post.models import NewLanguage


@pytest.fixture(scope="function")
def app():

    with tempfile.NamedTemporaryFile() as db_file:
        with tempfile.TemporaryDirectory() as media_dir:
            app = create_app(
                {
                    "TESTING": True,
                    "DATABASE_URI": "sqlite:///" + db_file.name,
                    "SECRET_KEY": "test",
                    "MEDIA_FOLDER": media_dir,
                    "RECAPTCHA_PUBLIC_KEY": (
                        "6Lfa12oeAAAAAA02TgOuOn4XPf9U7Qotguo73JJJ"
                    ),
                }
            )

            with app.app_context():
                init_db(echo=False)

                admin_user = User(
                    id=1,
                    email="admin@imca.edu.pe",
                    firstname="Pablo Gonzalo",
                    lastname1="Cárdenas",
                    lastname2="Barriga",
                    is_admin=1,
                    languages=[
                        UserLanguage(
                            lang_code="en",
                            summary="admin summary",
                        ),
                        UserLanguage(
                            lang_code="es",
                            summary="Resumen del admin",
                        ),
                    ],
                )
                researcher_user = User(
                    id=2,
                    email="test@imca.edu.pe",
                    firstname="Gonzalo",
                    lastname1="Panizo",
                    lastname2="García",
                    languages=[
                        UserLanguage(
                            lang_code="en",
                            summary="test summary",
                        ),
                        UserLanguage(
                            lang_code="es",
                            summary="Resumen del test",
                        ),
                    ],
                )
                graduated_student_user = User(
                    id=3,
                    email="other@imca.edu.pe",
                    firstname="Eladio",
                    lastname1="Ocaña",
                    lastname2="Anaya",
                    languages=[
                        UserLanguage(
                            lang_code="en",
                            summary="student summary",
                        ),
                        UserLanguage(
                            lang_code="es",
                            summary="Reseña del estudiante",
                        ),
                    ],
                )
                admin_researcher = Researcher(
                    user=admin_user,
                    is_associated=0,
                    languages=[
                        ResearcherLanguage(
                            lang_code="en",
                            field="admin field",
                        ),
                        ResearcherLanguage(
                            lang_code="es",
                            field="especialidad del  admin",
                        ),
                    ],
                )
                test_researcher = Researcher(
                    user=researcher_user,
                    is_associated=1,
                    languages=[
                        ResearcherLanguage(
                            lang_code="en",
                            field="test field",
                        ),
                        ResearcherLanguage(
                            lang_code="es",
                            field="Especialidad del test",
                        ),
                    ],
                    publications="""
@article{greenwade93,
    author  = "George D. Greenwade",
    title   = "The {C}omprehensive {T}ex {A}rchive {N}etwork ({CTAN})",
    year    = "1993",
    journal = "TUGBoat",
    volume  = "14",
    number  = "3",
    pages   = "342--351"
}
                    """,
                )

                research_area = ResearchArea(
                    user=researcher_user,
                    languages=[
                        ResearchAreaLanguage(
                            lang_code="en",
                            name="Probability",
                            summary="Probability summary",
                        ),
                        ResearchAreaLanguage(
                            lang_code="es",
                            name="Probability",
                            summary="Probability summary",
                        ),
                    ],
                    publications="""
@article{greenwade93,
    author  = "George D. Greenwade",
    title   = "The {C}omprehensive {T}ex {A}rchive {N}etwork ({CTAN})",
    year    = "1993",
    journal = "TUGBoat",
    volume  = "14",
    number  = "3",
    pages   = "342--351"
}
                    """,
                )

                local_postdate = datetime(2000, 1, 1, 0, 0, 0)
                new = New(
                    post_date=local_postdate + timedelta(hours=5),
                    expiration_date=local_postdate
                    + timedelta(weeks=1, hours=5),
                    carrousel=0,
                    languages=[
                        NewLanguage(
                            lang_code="en",
                            subtitle="subtitle in english",
                            subtype="announcement",
                            title="title in english",
                            body="body in english",
                        ),
                        NewLanguage(
                            lang_code="es",
                            subtitle="subtítulo en español",
                            subtype="comunicado",
                            title="título en español",
                            body="cuerpo en español",
                        ),
                    ],
                )
                event = Event(
                    post_date=local_postdate + timedelta(hours=5),
                    expiration_date=local_postdate
                    + timedelta(weeks=1, hours=5),
                    start_datetime=local_postdate + timedelta(hours=5),
                    end_datetime=local_postdate + timedelta(weeks=2, hours=5),
                    carrousel=0,
                    languages=[
                        EventLanguage(
                            lang_code="en",
                            subtype="talk",
                            title="event title in english",
                            body="body in english",
                            speaker="speaker",
                            location="location",
                        ),
                        EventLanguage(
                            lang_code="es",
                            subtype="charla",
                            title="evento título en español",
                            body="cuerpo en español",
                            speaker="Expositor",
                            location="ubicación",
                        ),
                    ],
                )

                flatpage = FlatPage(
                    languages=[
                        FlatPageLanguage(
                            lang_code="en",
                            title="flatpage title",
                            content="flatpage content",
                        ),
                        FlatPageLanguage(
                            lang_code="es",
                            title="flatpage título",
                            content="flatpage contenido",
                        ),
                    ],
                )

                program = Program(
                    is_active=1,
                    degree="master",
                    languages=[
                        ProgramLanguage(
                            lang_code="en",
                            degree="Master in",
                            title="Applied Mathematics",
                            body="Content of the master",
                        ),
                        ProgramLanguage(
                            lang_code="es",
                            degree="Maestría en",
                            title="Matemática Aplicada",
                            body="Contenido de la maestría",
                        ),
                    ],
                )

                graduated_student = GraduatedStudent(
                    user=graduated_student_user,
                    defense_date=date(2022, 4, 13),
                    program=program,
                    advisor="Advisor name",
                    external_advisor="External Advisor name",
                    title="Title of thesis",
                    supervisor1="supervisor1",
                    supervisor2="supervisor2",
                )

                db_session.add_all(
                    [
                        admin_user,
                        researcher_user,
                        graduated_student_user,
                        admin_researcher,
                        test_researcher,
                        research_area,
                        new,
                        event,
                        flatpage,
                        program,
                        graduated_student,
                    ]
                )
                db_session.commit()

        yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()


class AuthActions:
    def __init__(self, client, monkeypatch):
        self._client = client
        self._monkeypatch = monkeypatch

    def login(self, email="test@imca.edu.pe"):
        def fake_authorize_access_token():
            return {"userinfo": {"email": email}}

        self._monkeypatch.setattr(
            "flask_imca.multilingual.auth.oauth.google.authorize_access_token",
            fake_authorize_access_token,
        )

        return self._client.get("/en/auth/authorize")

    def logout(self):
        return self._client.get("/en/auth/logout")


@pytest.fixture
def auth(client, monkeypatch):
    return AuthActions(client, monkeypatch)
