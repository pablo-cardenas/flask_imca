from flask_babel import Locale
from pybtex.database import parse_string

from flask_imca.database import db_session
from flask_imca.multilingual.models import Researcher
from flask_imca.multilingual.models import User


def test_people_index(client):
    response = client.get("/en/people")
    assert response.status_code == 200


def test_researcher_detail(client, auth):
    response = client.get("/en/researcher/gonzalo-panizo-garcia")
    assert response.status_code == 200
    assert b">Edit<" not in response.data
    assert b"test field" in response.data
    assert b"test summary" in response.data
    assert b"Prof. Gonzalo Panizo" in response.data

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/researcher/gonzalo-panizo-garcia")
        assert response.status_code == 200
        assert b">Edit<" in response.data
        assert b"test field" in response.data
        assert b"test summary" in response.data
        assert b"Prof. Gonzalo Panizo" in response.data

        response = client.get("/en/researcher/pablo-gonzalo-cardenas-barriga")
        assert response.status_code == 200
        assert b">Edit<" not in response.data
        assert b"admin field" in response.data
        assert b"admin summary" in response.data
        assert bytes("Prof. Pablo Cárdenas", encoding="utf-8") in response.data

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/researcher/gonzalo-panizo-garcia")
        assert response.status_code == 200
        assert b">Edit<" in response.data
        assert b"test field" in response.data
        assert b"test summary" in response.data
        assert b"Prof. Gonzalo Panizo" in response.data

        response = client.get("/en/researcher/pablo-gonzalo-cardenas-barriga")
        assert response.status_code == 200
        assert b">Edit<" in response.data
        assert b"admin field" in response.data
        assert b"admin summary" in response.data


def test_researcher_update_login_required(client, auth):
    response = client.get("/en/researcher/2/update")
    assert response.status_code == 302
    assert response.headers["Location"] == (
        "/en/auth/login?next=%2Fen%2Fresearcher%2F2%2Fupdate"
    )

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/researcher/2/update")
        assert response.status_code == 200

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/researcher/2/update")
        assert response.status_code == 200


def test_researcher_update(client, auth, app):
    auth.login("test@imca.edu.pe")
    assert client.get("/en/researcher/2/update").status_code == 200

    response = client.post(
        "/en/researcher/2/update",
        data={
            "field-english": "updated field",
            "field-spanish": "especialidad actualizada",
            "summary-english": "updated summary",
            "summary-spanish": "reseña actualizada",
            "publications": """
@article{greenwade93,
    author  = "George D. Greenwade",
    title   = "The {C}omprehensive {T}ex {A}rchive {N}etwork ({CTAN})",
    year    = "2020",
    journal = "TUGBoat",
    volume  = "14",
    number  = "3",
    pages   = "342--351"
}
                    """,
        },
    )
    assert response.status_code == 302
    assert (
        response.headers["Location"] == "/en/researcher/gonzalo-panizo-garcia"
    )

    with app.app_context():
        researcher = db_session.get(Researcher, 2)
        assert (
            researcher.user.language(Locale("en")).summary == "updated summary"
        )
        assert (
            researcher.user.language(Locale("es")).summary
            == "reseña actualizada"
        )

        publications = parse_string(researcher.publications, "bibtex")
        assert publications.entries["greenwade93"].fields["year"] == "2020"


def test_graduated_student_detail(client, auth):
    response = client.get("/en/graduated-student/eladio-ocana-anaya")
    assert response.status_code == 200
    assert b">Edit<" not in response.data

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/graduated-student/eladio-ocana-anaya")
        assert response.status_code == 200
        assert b">Edit<" not in response.data

    auth.login("other@imca.edu.pe")
    with client:
        response = client.get("/en/graduated-student/eladio-ocana-anaya")
        assert response.status_code == 200
        assert b">Edit<" in response.data

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/graduated-student/eladio-ocana-anaya")
        assert response.status_code == 200
        assert b">Edit<" in response.data


def test_graduated_student_update_login_required(client, auth):
    response = client.get("/en/graduated-student/3/update")
    assert response.status_code == 302
    assert response.headers["Location"] == (
        "/en/auth/login?next=%2Fen%2Fgraduated-student%2F3%2Fupdate"
    )

    auth.login("other@imca.edu.pe")
    with client:
        response = client.get("/en/graduated-student/3/update")
        assert response.status_code == 200

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/graduated-student/3/update")
        assert response.status_code == 403

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/graduated-student/3/update")
        assert response.status_code == 200


def test_graduated_student_update(client, auth, app):
    auth.login("other@imca.edu.pe")
    assert client.get("/en/graduated-student/3/update").status_code == 200

    response = client.post(
        "/en/graduated-student/3/update",
        data={
            "summary-english": "updated summary",
            "summary-spanish": "reseña actualizada",
        },
    )
    assert response.status_code == 302
    assert (
        response.headers["Location"]
        == "/en/graduated-student/eladio-ocana-anaya"
    )

    with app.app_context():
        user = db_session.get(User, 3)
        assert user.language(Locale("en")).summary == "updated summary"
        assert user.language(Locale("es")).summary == "reseña actualizada"
