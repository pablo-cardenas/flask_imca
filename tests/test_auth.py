import pytest
from flask import g
from flask import session
from werkzeug.exceptions import BadRequestKeyError


def test_multilingual_login(client):
    response = client.get("/en/auth/login")
    assert response.status_code == 200
    assert b"/en/auth/login/google" in response.data


@pytest.mark.parametrize(
    ("query_string", "url_next"),
    (
        ("", None),
        ("?next=%2Ftest_next%2F", "/test_next/"),
    ),
)
def test_login_google(client, query_string, url_next):
    with client:
        response = client.get("/en/auth/login/google" + query_string)
        assert response.status_code == 302

        # Check that is not set to None
        assert not ("next" in session and session["next"] is None)

        assert session.get("next") == url_next


def test_authorize(client, auth):
    # Error when requesting auth.authorize without parameters
    try:
        response = client.get("/en/auth/authorize")
    except BadRequestKeyError:
        pass
    else:
        assert response.status_code == 400

    response = auth.login()
    assert response.headers["Location"] == "/"
    with client:
        client.get("/")
        print(session)
        assert session["user_id"] == 2
        assert g.user.email == "test@imca.edu.pe"

    # Check the next page when logging in
    with client.session_transaction() as sess:
        sess["next"] = "/test/"

    response = auth.login()
    assert response.headers["Location"] == "/test/"


def test_authorize_validate(client, auth):
    response = auth.login("none@imca.edu.pe")
    assert response.headers["Location"] == "/en/auth/login"
    with client:
        response = client.get("/en/auth/login")
        assert (
            b"is not registered. Check your email or contact with"
            in response.data
        )
        assert "user_id" not in session
        assert g.user is None

    response = auth.login("none@imca.edu.pe")
    assert response.headers["Location"] == "/en/auth/login"
    with client:
        response = client.get("/es/auth/login")
        assert (
            b"is not registered. Check your email or contact with"
            in response.data
        )
        assert "user_id" not in session
        assert g.user is None


def test_logout(client, auth):
    auth.login("test@imca.edu.pe")

    with client:
        response = client.get("/en/auth/logout")
        assert response.status_code == 302
        assert response.headers["Location"] == "/"
        assert "user_id" not in session
