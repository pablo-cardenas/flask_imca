from flask_babel import Locale

from flask_imca.database import db_session
from flask_imca.multilingual.models import FlatPage


def test_detail(client, auth):
    response = client.get("/en/page/flatpage-title")
    assert response.status_code == 200
    assert b">Edit<" not in response.data

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/page/flatpage-title")
        assert response.status_code == 200
        assert b">Edit<" not in response.data
    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/page/flatpage-title")
        assert response.status_code == 200
        assert b">Edit<" in response.data


def test_update_login_required(client, auth):
    response = client.get("/en/page/flatpage-title/update")
    assert response.status_code == 302
    assert response.headers["Location"] == (
        "/en/auth/login" "?next=%2Fen%2Fpage%2Fflatpage-title%2Fupdate"
    )

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/page/flatpage-title/update")
        assert response.status_code == 403

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/page/flatpage-title/update")
        assert response.status_code == 200


def test_update(client, auth, app):
    auth.login("admin@imca.edu.pe")

    response = client.get("/en/page/flatpage-title/update")
    response = client.post(
        "/en/page/flatpage-title/update",
        data={
            "content-english": "content updated",
            "content-spanish": "contenido actualizado",
        },
    )
    assert response.status_code == 302
    assert response.headers["Location"] == "/en/page/flatpage-title"

    with app.app_context():
        flatpage = db_session.get(FlatPage, 1)
        assert flatpage.language(Locale("en")).content == "content updated"
        assert (
            flatpage.language(Locale("es")).content == "contenido actualizado"
        )
