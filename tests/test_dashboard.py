def test_dashboard(client, auth):
    response = client.get("/en/dashboard")
    assert response.status_code == 302
    assert response.headers["Location"] == (
        "/en/auth/login" "?next=%2Fen%2Fdashboard"
    )
    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/dashboard")
        assert response.status_code == 403

    auth.login("admin@imca.edu.pe")
    with client:
        response = client.get("/en/dashboard")
        assert response.status_code == 200
