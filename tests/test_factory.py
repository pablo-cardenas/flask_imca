from flask_imca import create_app


def test_config():
    assert not create_app().testing
    assert create_app({"TESTING": True}).testing


def test_index(client):
    response = client.get("/")
    assert response.status_code == 302


def test_multilingual_index(client, auth):
    response = client.get("/en/")
    assert response.status_code == 200
    assert b"Log in" in response.data

    response = client.get("/es/")
    assert response.status_code == 200
    assert bytes("Iniciar sesión", encoding="utf-8") in response.data

    auth.login("test@imca.edu.pe")
    with client:
        response = client.get("/en/")
        assert response.status_code == 200
        assert b"Log out" in response.data
        assert b"Edit Researcher Profile" in response.data
        assert b"Edit Profile" not in response.data

        response = client.get("/es/")
        assert response.status_code == 200
        assert bytes("Cerrar sesión", encoding="utf-8") in response.data

    auth.login("other@imca.edu.pe")
    with client:
        response = client.get("/en/")
        assert response.status_code == 200
        assert b"Log out" in response.data
        assert b"Edit Profile" in response.data
        assert b"Edit Researcher Profile" not in response.data

        response = client.get("/es/")
        assert response.status_code == 200
        assert bytes("Cerrar sesión", encoding="utf-8") in response.data
