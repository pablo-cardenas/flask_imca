from flask_imca.database import get_db


def test_get_close_db(app):
    with app.app_context():
        db_sess = get_db()
        assert db_sess is get_db()


def test_init_db_command(runner, monkeypatch):
    class Recorder:
        called = False

    def fake_init_db():
        Recorder.called = True

    monkeypatch.setattr("flask_imca.database.init_db", fake_init_db)
    result = runner.invoke(args=["init-db"])
    assert result.output == "Initialized the database.\n"
    assert Recorder.called
