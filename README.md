# Flask IMCA

[[_TOC_]]

## Installation

### Local installation for development

#### Installation

Clone with git and change to the project directory
```sh
$ git clone https://gitlab.com/pablo-cardenas/flask_imca.git
$ cd flask_imca
```

Create a virtual environment and activate it.
Then, install the project in [editable mode](https://pip.pypa.io/en/stable/cli/pip_install/#install-editable).
```sh
$ virtualenv venv
$ source venv/bin/activate
$ pip install --editable .
```

Create a file `<INSTANCE-PATH>/config.py` (see [Instance path](#instance-path) and [Google OAuth](#google-oauth)).
If `DATABASE_URI` is not set, it defaults to `sqlite:///<INSTANCE-PATH>/imca.db`.
```
# config.py

DATABASE_URI = "..."
GOOGLE_CLIENT_ID = "..."
GOOGLE_CLIENT_SECRET = "..."
```

If you have a `dump.sql` file, load it into the database (it depends of the RDBMS).
If the RDBMS is sqlite, run
```sh
$ sqlite <DATABASE-PATH> < dump.sql
```

If a `dump.sql` was not loaded, you can create the schema on `DATABASE_URI` with the following command.
Then, populate the database.
```sh
$ export FLASK_APP=flask_imca
$ flask init-db
```


#### Development server

Start two terminals and set `FLASK_APP` environment variable on each terminal.
```sh
$ export FLASK_APP=flask_imca
```

In the other terminal, set `FLASK_ENV` environment variables and run flask development server.
```sh
$ export FLASK_ENV=development
$ flask run
```


### Heroku

#### Deploy app

Create a new empty application on Heroku
```sh
$ heroku create
```

Create the database
```sh
$ heroku addons:create heroku-postgresql:hobby-dev
```

If you have a `dump.sql` file, you can load it into the database.
```sh
$ heroku pg:psql < dump.sql
```

Add environment variables to Heroku. (See [Google OAuth](#google-oauth))
```sh
$ heroku config:set IS_HEROKU=True
$ heroku config:set SECRET_KEY=...
$ heroku config:set GOOGLE_CLIENT_ID=...
$ heroku config:set GOOGLE_CLIENT_SECRET=...
```

Add Python and Node.js buildpacks
```sh
$ heroku buildpacks:add heroku/python
$ heroku buildpacks:add heroku/nodejs
```

Deploy app to heroku by pushing to heroku remote.
```sh
$ git push heroku main
```

If a `dump.sql` file was not loaded, create the database.
```sh
$ heroku run FLASK_APP=flask_imca flask init-db
```

#### Destroy app

On the project directory, run
```sh
$ heroku apps:destroy
```


## Google OAuth

Get the `GOOGLE_CLIENT_ID` and `GOOGLE_CLIENT_SECRET` from [Google](console.cloud.google.com).


## Instance path

If project is installed in editable mode, the `<INSTANCE-PATH>` is `src/instance`.
Otherwise, it's `<ENV>/var/flask_imca-instance`.
